package com.example.teamorganizer.teo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
@NoArgsConstructor
public class EventOfScheduleDto {

    private Long schedule_Id;
    private Long teamId;
    private String typeEventName;
    private String TeamName;
    private List<PlayerDto> playerInformationList;
    private String information;

    public EventOfScheduleDto(Long schedule_Id, String typeEventName, String teamName, List<PlayerDto> playerInformationList, String information,Long teamId) {
        this.schedule_Id = schedule_Id;
        this.typeEventName = typeEventName;
        this.teamId = teamId;
        TeamName = teamName;
        this.playerInformationList = playerInformationList;
        this.information = information;
    }

    public EventOfScheduleDto(String information) {
        this.information = information;
    }
}
