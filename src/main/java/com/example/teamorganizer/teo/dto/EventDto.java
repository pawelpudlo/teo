package com.example.teamorganizer.teo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EventDto {

    private Long idSchedule;
    private Long idTeam;
    private String teamName;
    private Long idTypeEvent;
    private String nameTypeEvent;


    public EventDto(Long idSchedule, Long idTeam, String teamName, Long idTypeEvent, String nameTypeEvent) {
        this.idSchedule = idSchedule;
        this.idTeam = idTeam;
        this.teamName = teamName;
        this.idTypeEvent = idTypeEvent;
        this.nameTypeEvent = nameTypeEvent;
    }
}
