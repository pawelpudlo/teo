package com.example.teamorganizer.teo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PlayerStatisticDto {

    private Long idPlayer;
    private Long training;
    private Long matches;
    private Long goals;
    private Long assist;

    public PlayerStatisticDto(Long idPlayer, Long training, Long matches, Long goals, Long assist) {
        this.idPlayer = idPlayer;
        this.training = training;
        this.matches = matches;
        this.goals = goals;
        this.assist = assist;
    }

}
