package com.example.teamorganizer.teo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TeamDto {

    private Long teamId;
    private String teamName;
    private int numberOfPlayers;

    public TeamDto(Long teamId, String teamName, int numberOfPlayers) {
        this.teamId = teamId;
        this.teamName = teamName;
        this.numberOfPlayers = numberOfPlayers;
    }
}
