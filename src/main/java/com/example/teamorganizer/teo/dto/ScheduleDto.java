package com.example.teamorganizer.teo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ScheduleDto {

    private Long idSchedule;
    private Long idTeam;
    private String nameTeam;
    private String date;
    private String nameEvent;

    public ScheduleDto(Long idSchedule, Long idTeam, String nameTeam, String date, String nameEvent) {
        this.idSchedule = idSchedule;
        this.idTeam = idTeam;
        this.nameTeam = nameTeam;
        this.date = date;
        this.nameEvent = nameEvent;
    }
}
