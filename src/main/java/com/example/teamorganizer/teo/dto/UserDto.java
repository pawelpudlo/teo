package com.example.teamorganizer.teo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserDto {

    private Long idUser;
    private String login;
    private String password;
    private int role;

    public UserDto(Long idUser, String login, String password, int role) {
        this.idUser = idUser;
        this.login = login;
        this.password = password;
        this.role = role;
    }
}
