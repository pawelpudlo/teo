package com.example.teamorganizer.teo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class InformationDto {

    private String information;

    public InformationDto(String information) {
        this.information = information;
    }
}
