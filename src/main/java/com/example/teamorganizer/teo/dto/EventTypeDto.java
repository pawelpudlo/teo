package com.example.teamorganizer.teo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EventTypeDto {

    private Long idEvent;
    private String name;

    public EventTypeDto(Long idEvent, String name) {
        this.idEvent = idEvent;
        this.name = name;
    }
}
