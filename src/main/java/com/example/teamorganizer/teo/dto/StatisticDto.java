package com.example.teamorganizer.teo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class StatisticDto {

    private Long IdStatistic;
    private Long training;
    private Long matches;
    private Long goals;
    private Long assist;

    public StatisticDto(Long idStatistic, Long training, Long matches, Long goals, Long assist) {
        IdStatistic = idStatistic;
        this.training = training;
        this.matches = matches;
        this.goals = goals;
        this.assist = assist;
    }
}
