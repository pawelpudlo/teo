package com.example.teamorganizer.teo.dto.clientDto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class TeamNameDto {

    private String teamName;

    public TeamNameDto(String teamName) {
        this.teamName = teamName;
    }
}
