package com.example.teamorganizer.teo.dto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
@NoArgsConstructor
public class ForUpdateStatisticDto {

    private Long idSchedule;
    private Long idTypeEvent;
    private List<PlayerStatisticDto> playerDto;

    public ForUpdateStatisticDto(Long idSchedule, Long idTypeEvent, List<PlayerStatisticDto> playerDto) {
        this.idSchedule = idSchedule;
        this.idTypeEvent = idTypeEvent;
        this.playerDto = playerDto;
    }
}
