package com.example.teamorganizer.teo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@NoArgsConstructor
@Setter
@Getter
public class TeamsDto {

    private List<TeamDto> teams;

    public TeamsDto(List<TeamDto> teams) {
        this.teams = teams;
    }
}
