package com.example.teamorganizer.teo.dto;

import com.example.teamorganizer.teo.entity.TeamsEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PlayerDto {

    private Long idPlayer;
    private String name;
    private String familyName;
    private int age;
    private List<PlayerTeamDto> team;
    private List<StatisticDto> statistics;

    public PlayerDto(Long idPlayer, String name, String familyName, int age, List<PlayerTeamDto> team, List<StatisticDto> statistics) {
        this.idPlayer = idPlayer;
        this.name = name;
        this.familyName = familyName;
        this.age = age;
        this.team = team;
        this.statistics = statistics;
    }
}
