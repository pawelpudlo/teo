package com.example.teamorganizer.teo.service.impl;

import com.example.teamorganizer.teo.dto.*;
import com.example.teamorganizer.teo.mapper.PlayersMapper;
import com.example.teamorganizer.teo.mapper.ScheduleMapper;
import com.example.teamorganizer.teo.mapper.TeamsMapper;
import com.example.teamorganizer.teo.mapper.UsersMapper;
import com.example.teamorganizer.teo.service.TeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeoServiceImpl implements TeoService {

    private TeamsMapper teamsMapper;
    private UsersMapper usersMapper;
    private PlayersMapper playersMapper;
    private ScheduleMapper scheduleMapper;

    @Autowired
    public TeoServiceImpl(TeamsMapper teamsMapper, UsersMapper usersMapper,PlayersMapper playersMapper, ScheduleMapper scheduleMapper) {
        this.teamsMapper = teamsMapper;
        this.usersMapper = usersMapper;
        this.playersMapper = playersMapper;
        this.scheduleMapper = scheduleMapper;
    }

    @Override
    public Boolean addTeam(String team) {
        return this.teamsMapper.addTeam(team);
    }

    @Override
    public Long registerAccount(String login, String password) {
        return this.usersMapper.registerAccount(login, password);
    }

    @Override
    public Long loginAccount(String login, String password) {
        return this.usersMapper.loginAccount(login, password);
    }

    @Override
    public List<TeamDto> findAllTeams() {
        return teamsMapper.mapToDto();
    }

    @Override
    public TeamDto getSingleTeam(Long teamId) {
        return this.teamsMapper.mapSingleTeamToDto(teamId);
    }

    @Override
    public UserDto getUserRole(Long idUser) {
        return this.usersMapper.userRole(idUser);
    }

    @Override
    public Boolean deleteUser(Long idUser) {
        return this.usersMapper.deleteUser(idUser);
    }

    @Override
    public Boolean changeUserRole(Long idUser, int role) {
        return this.usersMapper.changeRange(idUser, role);
    }

    @Override
    public List<UserDto> getAllUsers() {
        return this.usersMapper.getAllUsers();
    }

    @Override
    public Boolean addPlayer(String name, String familyname, int age, Long idTeam) {
        return this.playersMapper.addPlayer(name,familyname,age,idTeam);
    }

    @Override
    public List<PlayerDto> getAllPlayersByTeam(Long teamId) {
        return this.playersMapper.getAllPlayers(teamId);
    }

    @Override
    public PlayerDto getSinglePlayerInformation(Long idPlayer) {
        return this.playersMapper.getSinglePlayer(idPlayer);
    }

    @Override
    public Boolean changePlayersTeam(Long idPlayer, Long idTeam) {
        return this.playersMapper.changeTeam(idPlayer, idTeam);
    }

    @Override
    public Boolean addSchedule(Long idTeam, String date, Long eventTypeId) {
        return this.scheduleMapper.addSchedule(idTeam,date,eventTypeId);
    }

    @Override
    public List<ScheduleDto> getSchedulesByTeam(Long idTeam) {
        return this.scheduleMapper.getSchedulesByTeam(idTeam);
    }

    @Override
    public List<EventTypeDto> getAllEvents() {
        return this.scheduleMapper.getAllEvents();
    }

    @Override
    public Boolean changeEventOfSchedule(Long idSchedule, Long idType, Long idTeam, String date) {
        return this.scheduleMapper.changeEventOfSchedule(idSchedule, idType, idTeam, date);
    }

    @Override
    public Boolean deleteEventOfSchedule(Long idSchedule) {
        return this.scheduleMapper.deleteEventOfSchedule(idSchedule);
    }

    @Override
    public Boolean deletePlayerOfTeam(Long idPlayer, Long idTeam) {
        return this.playersMapper.deletePlayerOfTeam(idPlayer, idTeam);
    }

    @Override
    public EventOfScheduleDto getEventOfSchedules(Long schedule_id) {
        return this.scheduleMapper.getEventOfSchedules(schedule_id);
    }

    @Override
    public Boolean changePlayerStatistics(Long idPlayer,Long traning, Long match, Long assists, Long goals) {
        return this.playersMapper.changePlayerStatistics(idPlayer, traning,match, assists, goals);
    }

    @Override
    public List<EventDto> getAllScheduleEvent() {
        return this.scheduleMapper.getAllEventsOfSchedule();
    }

    @Override
    public Boolean changeStatistic(ForUpdateStatisticDto forUpdateStatisticDto) {
        return this.scheduleMapper.updateStatistic(forUpdateStatisticDto);
    }

}

