package com.example.teamorganizer.teo.service;

import com.example.teamorganizer.teo.dto.*;

import java.util.List;


public interface TeoService {

    Boolean addTeam(String team);

    Long registerAccount(String login, String password);

    Long loginAccount(String login, String password);

    List<TeamDto> findAllTeams();

    TeamDto getSingleTeam(Long teamId);

    UserDto getUserRole(Long idUser);

    Boolean deleteUser(Long teamId);

    Boolean changeUserRole(Long idUser, int role);

    List<UserDto> getAllUsers();

    Boolean addPlayer(String name,String familyname, int age,Long idTeam);

    List<PlayerDto> getAllPlayersByTeam(Long teamId);

    PlayerDto getSinglePlayerInformation(Long idPlayer);

    Boolean changePlayersTeam(Long idPlayer, Long idTeam);

    Boolean addSchedule(Long idTeam,String date,Long eventTypeId);

    List<ScheduleDto> getSchedulesByTeam(Long idTeam);

    List<EventTypeDto> getAllEvents();

    Boolean changeEventOfSchedule(Long idSchedule, Long idType, Long idTeam, String date);

    Boolean deleteEventOfSchedule(Long idSchedule);

    Boolean deletePlayerOfTeam(Long idPlayer, Long idTeam);

    EventOfScheduleDto getEventOfSchedules(Long schedule_id);

    Boolean changePlayerStatistics(Long idPlayer, Long traning, Long match, Long assists, Long goals);

    List<EventDto> getAllScheduleEvent();

    Boolean changeStatistic(ForUpdateStatisticDto forUpdateStatisticDto);

}

