package com.example.teamorganizer.teo;

import com.example.teamorganizer.teo.entity.*;
import com.example.teamorganizer.teo.repository.*;
import com.example.teamorganizer.teo.until.DateSqlConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class Start {

    private PlayersRepository playersRepository;
    private PlayersStatisticRepository playersStatisticRepository;
    private TeamsRepository teamsRepository;
    private TeamPlayersRepository teamPlayersRepository;
    private EventTypeRepository eventTypeRepository;
    private ScheduleRepository scheduleRepository;
    private UsersRepository usersRepository;

    public Start(PlayersRepository playersRepository, PlayersStatisticRepository playersStatisticRepository, TeamsRepository teamsRepository, TeamPlayersRepository teamPlayersRepository, EventTypeRepository eventTypeRepository, ScheduleRepository scheduleRepository, UsersRepository usersRepository) {
        this.playersRepository = playersRepository;
        this.playersStatisticRepository = playersStatisticRepository;
        this.teamsRepository = teamsRepository;
        this.teamPlayersRepository = teamPlayersRepository;
        this.eventTypeRepository = eventTypeRepository;
        this.scheduleRepository = scheduleRepository;
        this.usersRepository = usersRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void runExample(){

        DateSqlConverter dateSqlConverter = new DateSqlConverter();
        Timestamp date = dateSqlConverter.getTimestampDate("2016-04-06T10:02:23-02:00");

        usersRepository.save(new UsersEntity("admin","admin",1));
        usersRepository.save(new UsersEntity("adek","adek",1));

        playersRepository.save(new PlayersEntity("Adam","Kawa",33));
        playersRepository.save(new PlayersEntity("Olek","Kula",21));
        playersRepository.save(new PlayersEntity("Marcin","Najman",17));

        playersStatisticRepository.save(new PlayersStatisticEntity(1L,0L,0L,0L,0L));
        playersStatisticRepository.save(new PlayersStatisticEntity(2L,0L,0L,0L,0L));
        playersStatisticRepository.save(new PlayersStatisticEntity(3L,0L,0L,0L,0L));


        teamsRepository.save(new TeamsEntity("Legia Warszawa",2));
        teamsRepository.save(new TeamsEntity("Najman FC",1));

        teamPlayersRepository.save(new TeamPlayersEntity(1L,1L));
        teamPlayersRepository.save(new TeamPlayersEntity(1L,2L));
        teamPlayersRepository.save(new TeamPlayersEntity(2L,3L));

        eventTypeRepository.save(new EventTypeEntity("Mecz"));
        eventTypeRepository.save(new EventTypeEntity("Trening"));

        scheduleRepository.save(new SchedulesEntity(1L,date,1L));
        scheduleRepository.save(new SchedulesEntity(2L,date,2L));
        scheduleRepository.save(new SchedulesEntity(2L,date,1L));
        scheduleRepository.save(new SchedulesEntity(2L,date,2L));

    }

}
