package com.example.teamorganizer.teo.mapper;

import com.example.teamorganizer.teo.dto.*;
import com.example.teamorganizer.teo.entity.*;
import com.example.teamorganizer.teo.repository.*;
import com.example.teamorganizer.teo.until.DateSqlConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Component
public class ScheduleMapper {

    private TeamsRepository teamsRepository;
    private EventTypeRepository eventTypeRepository;
    private ScheduleRepository scheduleRepository;
    private TeamPlayersRepository teamPlayersRepository;
    private PlayersStatisticRepository playersStatisticRepository;
    private PlayersRepository playersRepository;

    @Autowired
    public ScheduleMapper(TeamsRepository teamsRepository, EventTypeRepository eventTypeRepository, ScheduleRepository scheduleRepository, TeamPlayersRepository teamPlayersRepository, PlayersStatisticRepository playersStatisticRepository, PlayersRepository playersRepository) {
        this.teamsRepository = teamsRepository;
        this.eventTypeRepository = eventTypeRepository;
        this.scheduleRepository = scheduleRepository;
        this.teamPlayersRepository = teamPlayersRepository;
        this.playersStatisticRepository = playersStatisticRepository;
        this.playersRepository = playersRepository;
    }

    public boolean addSchedule(Long idTeam, String date, Long eventTypeId){

        if(date==null || idTeam==null || eventTypeId == null) return false;

        Timestamp timestampDate = new DateSqlConverter().getTimestampDate(date);

        try {

            Long teamId = this.teamsRepository.getTeamId(idTeam);
            Long idEvent = this.eventTypeRepository.getEventId(eventTypeId);

            this.scheduleRepository.save(new SchedulesEntity(teamId,timestampDate,idEvent));

            return true;
        }catch (Exception var4){
            return false;
        }
    }

    public List<ScheduleDto> getSchedulesByTeam(long idTeam){

        List<ScheduleDto> schedules = new ArrayList<>();
        List<SchedulesEntity> schedulesEntity = this.scheduleRepository.getSchedulesEntityByIdTeam(idTeam);
        TeamsEntity teamsEntity = this.teamsRepository.getOneByTeamId(idTeam);

        for(SchedulesEntity se : schedulesEntity){

            String date = se.getDate().toString();
            String eventName = this.eventTypeRepository.getEventName(se.getIdEventType());

            ScheduleDto scheduleDto = new ScheduleDto(se.getIdSchedules(), idTeam, teamsEntity.getNameTeam(), date, eventName);

            schedules.add(scheduleDto);
        }

        return schedules;
    }

    public List<EventTypeDto> getAllEvents(){

        List<EventTypeEntity> allEventsTypes = this.eventTypeRepository.getAllEventsTypes();
        List<EventTypeDto> events = new ArrayList<>();

        for(EventTypeEntity ete : allEventsTypes){

            EventTypeDto eventTypeDto = new EventTypeDto(ete.getIdEvent(), ete.getName());

            events.add(eventTypeDto);
        }

        return events;
    }

    public Boolean changeEventOfSchedule(Long idSchedule, Long idType, Long idTeam, String date){

        if(idSchedule == null || idType == null || date == null || idTeam == null) return false;

        try{
            SchedulesEntity schedulesEntity = this.scheduleRepository.getScheduleEntityByIdSchedules(idSchedule);

            Timestamp timestampDate = new DateSqlConverter().getTimestampDate(date);

            schedulesEntity.setIdTeam(idTeam);
            schedulesEntity.setDate(timestampDate);
            schedulesEntity.setIdEventType(idType);

            scheduleRepository.save(schedulesEntity);

            return true;
        }catch (Exception var4){
            return false;
        }
    }

    public Boolean deleteEventOfSchedule(Long idSchedule){

        try{
            scheduleRepository.deleteById(idSchedule);

            return true;
        }catch (Exception var4){
            return false;
        }
    }

    public EventOfScheduleDto getEventOfSchedules(Long schedule_id){

        List<PlayerDto> listPlayers = new ArrayList();
        List<PlayerTeamDto> listPlayerTeam = new ArrayList();
        try{

            Long idSchedule = this.scheduleRepository.getScheduleEntityByIdSchedules(schedule_id).getIdSchedules();

            Long typeId = this.scheduleRepository.getScheduleEntityByIdSchedules(schedule_id).getIdEventType();

            String typeName = this.eventTypeRepository.getEventName(typeId);

            String teamName = this.teamsRepository.getOneByTeamId(this.scheduleRepository.getScheduleEntityByIdSchedules(schedule_id).getIdTeam()).getNameTeam();

            Long teamId = this.scheduleRepository.getScheduleEntityByIdSchedules(schedule_id).getIdTeam();

            String information = "Poprawnie wzrócono zawodnika";

            PlayerTeamDto playerTeamDto = new PlayerTeamDto(teamId,teamName);

            listPlayerTeam.add(playerTeamDto);

            List<TeamPlayersEntity> listPlayersOfTeam = this.teamPlayersRepository.getAllPlayersByTeam(teamId);


            for (TeamPlayersEntity playerOfTeam:listPlayersOfTeam) {

                List<StatisticDto> statisticDto = new ArrayList();
                List<PlayerTeamDto> listPlayerTeamInfo = new ArrayList();

                PlayersEntity playersEntity = this.playersRepository.getPlayerPersonalInformation(playerOfTeam.getIdPlayer());
                Long idPlayer = playersEntity.getIdPlayer();
                String name = playersEntity.getName();
                String familyName = playersEntity.getFamilyName();
                int age = playersEntity.getAge();

                PlayersStatisticEntity playersStatisticEntity = this.playersStatisticRepository.getPlayerStatistics(playerOfTeam.getIdPlayer());
                statisticDto.add(new StatisticDto(playersStatisticEntity.getIdStatistic(),playersStatisticEntity.getTraining(),playersStatisticEntity.getMatches(),playersStatisticEntity.getGoals(),playersStatisticEntity.getAssist()));
                listPlayerTeamInfo.add(new PlayerTeamDto(teamId,teamName));


                listPlayers.add(new PlayerDto(idPlayer,name,familyName,age,listPlayerTeamInfo,statisticDto) );

            }

            return new EventOfScheduleDto(idSchedule,typeName,teamName,listPlayers,information,teamId);
        }catch (Exception var4){
            return new EventOfScheduleDto("404 ERROR");
        }
    }

    public List<EventDto> getAllEventsOfSchedule(){

        List<EventDto> listEventDto = new ArrayList();

        try{

            List<SchedulesEntity> listSchedule = this.scheduleRepository.getSchedulesEntity();

            for(SchedulesEntity  event:listSchedule){

                Long idSchedule = event.getIdSchedules();
                Long idTeam = event.getIdTeam();
                Long idTypeEvent = event.getIdEventType();

                String teamName = this.teamsRepository.getTeamInfoById(idTeam).getNameTeam();
                String nameTypeEvent = this.eventTypeRepository.getEventName(idTypeEvent);

                listEventDto.add(new EventDto(idSchedule,idTeam,teamName,idTypeEvent,nameTypeEvent));
            }
            return listEventDto;
        }catch(Exception var4){
            listEventDto.clear();
            return listEventDto;
        }
    }

    public Boolean updateStatistic(ForUpdateStatisticDto forUpdateStatisticDto){
        try {

            SchedulesEntity scheduleInformation = this.scheduleRepository.getScheduleEntityByIdSchedules(forUpdateStatisticDto.getIdSchedule());
            Long teamId = scheduleInformation.getIdTeam();
            Long eventTypeId = scheduleInformation.getIdEventType();

            Long idTypeEvent = this.eventTypeRepository.getEventId(eventTypeId);
            Long idTeam = this.teamsRepository.getTeamId(teamId);

            String eventName = this.eventTypeRepository.getEventName(idTypeEvent).toUpperCase();

            for (PlayerStatisticDto  playersInfo: forUpdateStatisticDto.getPlayerDto() ) {
                Long id = this.teamPlayersRepository.getIdForCheck(playersInfo.getIdPlayer(),idTeam);
                if(id == null) return false;
            }

            List<PlayersStatisticEntity> statisticPlayerForUpdate = new ArrayList();

            for(PlayerStatisticDto  playersInfo: forUpdateStatisticDto.getPlayerDto()){
                PlayersStatisticEntity playerStatistic = this.playersStatisticRepository.getPlayerStatistics(playersInfo.getIdPlayer());

                if(eventName.equals("MECZ")){

                    playerStatistic.setMatches(playersInfo.getMatches() + playerStatistic.getMatches());
                    playerStatistic.setGoals(playersInfo.getGoals() + playerStatistic.getGoals());
                    playerStatistic.setAssist(playersInfo.getAssist() + playerStatistic.getAssist());

                    statisticPlayerForUpdate.add(playerStatistic);
                }else if(eventName.equals("TRENING")){

                    playerStatistic.setTraining(playersInfo.getTraining() + playerStatistic.getTraining());
                    statisticPlayerForUpdate.add(playerStatistic);
                }else{
                    return false;
                }

            }

            for(PlayersStatisticEntity statisticForSave:statisticPlayerForUpdate){
                System.out.println(statisticForSave);
            }


                return true;
        }catch (Exception var4){
            return false;
        }
    }

}
