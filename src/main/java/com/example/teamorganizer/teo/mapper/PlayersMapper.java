package com.example.teamorganizer.teo.mapper;

import com.example.teamorganizer.teo.dto.PlayerDto;
import com.example.teamorganizer.teo.entity.*;
import com.example.teamorganizer.teo.repository.*;
import com.example.teamorganizer.teo.until.CheckingRegularExpresion;
import com.example.teamorganizer.teo.until.GetPlayersInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PlayersMapper {

    private PlayersRepository playersRepository;
    private PlayersStatisticRepository playersStatisticRepository;
    private TeamPlayersRepository teamPlayersRepository;
    private TeamsRepository teamsRepository;
    private ScheduleRepository scheduleRepository;

    @Autowired
    public PlayersMapper(PlayersRepository playersRepository, PlayersStatisticRepository playersStatisticRepository, TeamPlayersRepository teamPlayersRepository, TeamsRepository teamsRepository, ScheduleRepository scheduleRepository) {
        this.playersRepository = playersRepository;
        this.playersStatisticRepository = playersStatisticRepository;
        this.teamPlayersRepository = teamPlayersRepository;
        this.teamsRepository = teamsRepository;
        this.scheduleRepository = scheduleRepository;
    }
    public boolean addPlayer(String name, String familyname, int age, Long idTeam) {

        CheckingRegularExpresion checkingRegularExpresion = new CheckingRegularExpresion();

        try {

            if (!checkingRegularExpresion.checkWord(name) || !checkingRegularExpresion.checkWord(familyname))
                return false;

            Long teamId = this.teamsRepository.getTeamId(idTeam);

            if (teamId == null) return false;

            this.playersRepository.save(new PlayersEntity(name, familyname, age));

            Long playerId = this.playersRepository.getIdNewPlayer(name, familyname);

            this.teamPlayersRepository.save(new TeamPlayersEntity(teamId, playerId));

            this.playersStatisticRepository.save(new PlayersStatisticEntity(playerId, 0L, 0L, 0L, 0L));

            int valuePlayersInTeam = this.teamsRepository.getValuePlayerInTeam(teamId);
            valuePlayersInTeam++;

            TeamsEntity teamEntity = teamsRepository.getOneByTeamId(teamId);

            teamEntity.setNumbersOfPlayers(valuePlayersInTeam);

            teamsRepository.save(teamEntity);

            return true;
        } catch (Exception var4) {
            return false;
        }

    }

    public List<PlayerDto> getAllPlayers(long idTeam) {
        List<PlayerDto> playersDto = new ArrayList<>();
        List<TeamPlayersEntity> teamPlayersEntityList = this.teamPlayersRepository.getAllPlayersByTeam(idTeam);

        for (TeamPlayersEntity singlePlayer : teamPlayersEntityList) {
            GetPlayersInformation getPlayersInformation = new GetPlayersInformation();
            PlayerDto playerDto = getPlayersInformation.getPlayer(this.playersRepository, this.playersStatisticRepository, this.teamsRepository, singlePlayer);

            playersDto.add(playerDto);
        }

        return playersDto;
    }

    public PlayerDto getSinglePlayer(long idPlayer) {

        try {
            TeamPlayersEntity teamPlayersEntity = this.teamPlayersRepository.getTeamByPlayer(idPlayer);
            GetPlayersInformation getPlayersInformation = new GetPlayersInformation();

            return getPlayersInformation.getPlayer(this.playersRepository, this.playersStatisticRepository, this.teamsRepository, teamPlayersEntity);
        } catch (Exception var4) {
            return new PlayerDto();
        }

    }

    public Boolean changeTeam(long idPlayer, long idTeam) {

        try{
            TeamPlayersEntity teamPlayersEntity = this.teamPlayersRepository.getTeamByPlayer(idPlayer);

            if (idTeam == teamPlayersEntity.getIdTeam()) return false;

            TeamsEntity oldTeam = teamsRepository.getOneByTeamId(teamPlayersEntity.getIdTeam());
            int valueOfOldTeam = this.teamsRepository.getValuePlayerInTeam(oldTeam.getIdTeam());
            valueOfOldTeam--;
            oldTeam.setNumbersOfPlayers(valueOfOldTeam);

            int valueOfNewTeam = this.teamsRepository.getValuePlayerInTeam(idTeam);
            valueOfNewTeam++;
            TeamsEntity newTeam = teamsRepository.getOneByTeamId(idTeam);
            newTeam.setNumbersOfPlayers(valueOfNewTeam);

            PlayersStatisticEntity playersStatisticEntity = this.playersStatisticRepository.getPlayerStatistics(idPlayer);
            playersStatisticEntity.setTraining(0L);
            playersStatisticEntity.setMatches(0L);
            playersStatisticEntity.setGoals(0L);
            playersStatisticEntity.setAssist(0L);

            teamPlayersEntity.setIdTeam(idTeam);

            playersStatisticRepository.save(playersStatisticEntity);
            teamsRepository.save(oldTeam);
            teamsRepository.save(newTeam);
            teamPlayersRepository.save(teamPlayersEntity);

            return true;
        }catch (Exception var4){
            return false;
        }
    }

    public Boolean deletePlayerOfTeam(Long idPlayer, Long idTeam){

        if(idPlayer == null || idTeam == null) return false;

        try {

            this.playersStatisticRepository.deletePlayersStatisticEntitiesByIdPlayer(idPlayer);
            this.teamPlayersRepository.deleteByIdPlayerAndAndIdTeam(idPlayer, idTeam);
            this.playersRepository.deleteById(idPlayer);

            TeamsEntity team = this.teamsRepository.getOneByTeamId(idTeam);

            int numberOfPlayer = team.getNumbersOfPlayers() - 1;
            team.setNumbersOfPlayers(numberOfPlayer);

            this.teamsRepository.save(team);

            return true;
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return false;
        }
    }

    public Boolean changePlayerStatistics(Long idPlayer, Long training, Long match , Long assists, Long goals){

        try {

            PlayersStatisticEntity playersStatisticEntity = this.playersStatisticRepository.getPlayerStatistics(idPlayer);

            playersStatisticEntity.setAssist(assists);
            playersStatisticEntity.setGoals(goals);
            playersStatisticEntity.setMatches(match);
            playersStatisticEntity.setTraining(training);
            this.playersStatisticRepository.save(playersStatisticEntity);
            return true;
        }catch (Exception var4){
            return false;
        }
    }

}
