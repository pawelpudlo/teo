package com.example.teamorganizer.teo.mapper;

import com.example.teamorganizer.teo.dto.TeamDto;
import com.example.teamorganizer.teo.dto.UserDto;
import com.example.teamorganizer.teo.entity.TeamsEntity;
import com.example.teamorganizer.teo.entity.UsersEntity;
import com.example.teamorganizer.teo.repository.UsersRepository;
import com.example.teamorganizer.teo.until.CheckingRegularExpresion;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UsersMapper {

    private UsersRepository usersRepository;

    @Autowired
    public UsersMapper(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public Long registerAccount(String newUserLogin, String password) {

        try {
            if (newUserLogin.length() < 3 || password.length() < 3) return 0L; // password strength

            CheckingRegularExpresion checkingRegularExpresion = new CheckingRegularExpresion();

            if (!checkingRegularExpresion.checkWord(newUserLogin)) return 0L;
            if (!checkingRegularExpresion.checkWord(password)) return 0L;

            List<String> usersLogin = this.usersRepository.getAllUsers();

            for (String userLogin : usersLogin) {

                String lowerWord = userLogin.toLowerCase();

                if (lowerWord.equals(newUserLogin.toLowerCase())) {
                    return 0L;
                }
            }

            UsersEntity usersEntity = usersRepository.save(new UsersEntity(newUserLogin, password, 0));

            return usersEntity.getIdUser();

        } catch (Exception var4) {
            return 0L;
        }
    }

    public Long loginAccount(String newUserLogin, String password) {
        try {
            if (newUserLogin.length() < 3 || password.length() < 3) return 0L; // password strength

            CheckingRegularExpresion checkingRegularExpresion = new CheckingRegularExpresion();

            if (!checkingRegularExpresion.checkWord(newUserLogin)) return 0L;

            if (!checkingRegularExpresion.checkWord(password)) return 0L;

            List<UsersEntity> usersData = this.usersRepository.getAllUsersWithPassowrd();

            for (UsersEntity users : usersData) {
                if (users.getLogin().equals(newUserLogin) && users.getPassword().equals(password)) return users.getIdUser();
            }

            return 0L;
        } catch (Exception var4) {
            return 0L;
        }
    }

    public UserDto userRole(long idUser) {
        try {
            UsersEntity usersEntity = usersRepository.findOneByIdUser(idUser);
            return new UserDto(usersEntity.getIdUser(), usersEntity.getLogin(), usersEntity.getPassword(), usersEntity.getRole());
        } catch (Exception var4) {
            return new UserDto();
        }
    }

    public boolean deleteUser(long idUser) {
        try {
            usersRepository.deleteById(idUser);
            return true;
        } catch (Exception var4) {
            return false;
        }
    }

    public boolean changeRange(long idUser, int role) {
        try {
            UsersEntity usersEntity = usersRepository.findOneByIdUser(idUser);
            if (role == 0) {
                usersEntity.setRole(1);
            } else if (role == 1) {
                usersEntity.setRole(0);
            }
            usersRepository.save(usersEntity);
            return true;
        } catch (Exception var4) {
            return false;
        }
    }

    public List<UserDto> getAllUsers() {
        List<UsersEntity> users = usersRepository.getAllUsersWithAllInfromation();
        List<UserDto> usersDto = new ArrayList<>();
        for (UsersEntity usersEntity : users) {
            UserDto userDto = new UserDto();

            userDto.setIdUser(usersEntity.getIdUser());
            userDto.setLogin(usersEntity.getLogin());
            userDto.setPassword(usersEntity.getPassword());
            userDto.setRole(usersEntity.getRole());

            usersDto.add(userDto);
        }
        return usersDto;
    }

}

