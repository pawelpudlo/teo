package com.example.teamorganizer.teo.mapper;

import com.example.teamorganizer.teo.dto.TeamDto;
import com.example.teamorganizer.teo.entity.TeamsEntity;
import com.example.teamorganizer.teo.repository.TeamsRepository;
import com.example.teamorganizer.teo.until.CheckingRegularExpresion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.*;
@Component
public class TeamsMapper {

    private TeamsRepository teamsRepository;

    @Autowired
    public TeamsMapper(TeamsRepository teamsRepository) {
        this.teamsRepository = teamsRepository;
    }

    public Boolean addTeam(String teamName){
        try {
            CheckingRegularExpresion checkingRegularExpresion = new CheckingRegularExpresion();
            if(!checkingRegularExpresion.checkWord(teamName)) return false;
            List<String> teamsName = this.teamsRepository.getAllTeams();
            for (String name:teamsName) {
                String lowCaseName = name.toLowerCase();
                if(lowCaseName.equals(teamName.toLowerCase())){
                    return false;
                }
            }
            this.teamsRepository.save(new TeamsEntity(teamName,0));
            return true;
        }catch (Exception var4){
            return false;
        }

    }

    public List<TeamDto> mapToDto(){
        List<TeamsEntity> teams = teamsRepository.getAllTeamsWithAllInformation();
        List<TeamDto> teamsDto = new ArrayList<>();
        for(TeamsEntity teamsEntity: teams){
            TeamDto teamDto = new TeamDto();

            teamDto.setTeamId(teamsEntity.getIdTeam());
            teamDto.setTeamName(teamsEntity.getNameTeam());
            teamDto.setNumberOfPlayers(teamsEntity.getNumbersOfPlayers());

            teamsDto.add(teamDto);
        }
        return teamsDto;
    }

    public TeamDto mapSingleTeamToDto(long teamId){
        TeamsEntity teamsEntity = teamsRepository.findOneByIdTeam(teamId);
        try{
            return new TeamDto(teamsEntity.getIdTeam(), teamsEntity.getNameTeam(), teamsEntity.getNumbersOfPlayers());
        }catch (Exception var4){
            return new TeamDto();
        }
    }

}
