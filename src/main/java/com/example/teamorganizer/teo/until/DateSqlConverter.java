package com.example.teamorganizer.teo.until;

import java.sql.Timestamp;
import org.joda.time.DateTime;

public class DateSqlConverter {

    public DateSqlConverter() {
    }

    public Timestamp getTimestampDate(String date){
            if(date == null || date.length() != 25) return Timestamp.valueOf("1970-01-01 01:01:01");
            DateTime dateTime = new DateTime(date);

            String newDate = dateTime.getYear() + "-"
                    + dateTime.getMonthOfYear() + "-"
                    + dateTime.getDayOfMonth() + " "
                    + getValueWithZero(dateTime.getHourOfDay()) + ":"
                    + getValueWithZero(dateTime.getMinuteOfHour()) + ":"
                    + getValueWithZero(dateTime.getSecondOfMinute());

            return Timestamp.valueOf(newDate);
    }


    private String getValueWithZero(int value){
        if(value < 10){
            return "0" + value;
        }else{
            return "" + value;
        }


}
}
