package com.example.teamorganizer.teo.until;

import com.example.teamorganizer.teo.dto.PlayerDto;
import com.example.teamorganizer.teo.dto.PlayerTeamDto;
import com.example.teamorganizer.teo.dto.StatisticDto;
import com.example.teamorganizer.teo.entity.PlayersEntity;
import com.example.teamorganizer.teo.entity.PlayersStatisticEntity;
import com.example.teamorganizer.teo.entity.TeamPlayersEntity;
import com.example.teamorganizer.teo.entity.TeamsEntity;
import com.example.teamorganizer.teo.repository.PlayersRepository;
import com.example.teamorganizer.teo.repository.PlayersStatisticRepository;
import com.example.teamorganizer.teo.repository.TeamsRepository;

import java.util.ArrayList;
import java.util.List;

public class GetPlayersInformation {

    public GetPlayersInformation() {
    }

    public PlayerDto getPlayer(PlayersRepository playersRepository, PlayersStatisticRepository playersStatisticRepository, TeamsRepository teamsRepository, TeamPlayersEntity singlePlayer){

        StatisticDto statisticDto = new StatisticDto();
        PlayerDto playerDto = new PlayerDto();
        PlayerTeamDto playerTeamDto = new PlayerTeamDto();
        List<StatisticDto> statisticsDto = new ArrayList<>();
        List<PlayerTeamDto> playerTeam = new ArrayList<>();
        TeamsEntity teamsEntity = teamsRepository.getTeamInfoById(singlePlayer.getIdTeam());
        PlayersEntity playerPersonalInformation = playersRepository.getPlayerPersonalInformation(singlePlayer.getIdPlayer());
        PlayersStatisticEntity playerStatistic = playersStatisticRepository.getPlayerStatistics(singlePlayer.getIdPlayer());

        playerTeamDto.setTeamId(teamsEntity.getIdTeam());
        playerTeamDto.setTeamName(teamsEntity.getNameTeam());
        playerTeam.add(playerTeamDto);

        statisticDto.setIdStatistic(playerStatistic.getIdStatistic());
        statisticDto.setAssist(playerStatistic.getAssist());
        statisticDto.setGoals(playerStatistic.getGoals());
        statisticDto.setMatches(playerStatistic.getMatches());
        statisticDto.setTraining(playerStatistic.getTraining());
        statisticsDto.add(statisticDto);

        playerDto.setIdPlayer(playerPersonalInformation.getIdPlayer());
        playerDto.setName(playerPersonalInformation.getName());
        playerDto.setFamilyName(playerPersonalInformation.getFamilyName());
        playerDto.setAge(playerPersonalInformation.getAge());
        playerDto.setTeam(playerTeam);
        playerDto.setStatistics(statisticsDto);

        return playerDto;
    }
}
