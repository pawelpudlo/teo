package com.example.teamorganizer.teo.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;

public class JwtFilter implements javax.servlet.Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {


        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        System.out.println(httpServletRequest.toString());
        Enumeration<String> headers = httpServletRequest.getHeaderNames();
        if (headers != null) {
            while (headers.hasMoreElements()) {
                System.out.println("Header: " + httpServletRequest.getHeader(headers.nextElement()));
            }
        }
        System.out.println("====== END PARAMS ======");

        String header = httpServletRequest.getHeader("authorization");

        if(header == null || !header.startsWith("Bearer")){
            throw new ServletException(("Wrong or empty header"));
        }else{
            try{
                String token =  header.substring(7);
                Claims claims = Jwts.parser().setSigningKey("admin").parseClaimsJws(token).getBody();
                servletRequest.setAttribute("claims",claims);
            }catch (Exception ex){
                throw new ServletException("Wrong key");
            }

        }
        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {

    }
}
