package com.example.teamorganizer.teo.jwt;

import com.example.teamorganizer.teo.dto.InformationDto;
import com.example.teamorganizer.teo.service.TeoService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;

@RestController
public class LoginApi {


    private TeoService teoService;

    @Autowired
    public LoginApi(TeoService teoService) {
        this.teoService = teoService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/logIn")
    public ResponseEntity<InformationDto> get(@RequestBody User user){

        Long id = this.teoService.loginAccount(user.getLogin(), user.getPassword());

        long currentTimeMillis = System.currentTimeMillis();
        InformationDto informationDto = new InformationDto(Jwts.builder()
                .setSubject(user.getLogin())
                .claim("roles","admin")
                .setIssuedAt(new Date(currentTimeMillis))
                .setExpiration(new Date(currentTimeMillis + 20000))
                .signWith(SignatureAlgorithm.HS512, user.getPassword())
                .compact());

        if(id!=0){
            return new ResponseEntity<>(informationDto,HttpStatus.OK);
        }else{
            return new ResponseEntity<>(informationDto,HttpStatus.CONFLICT);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/registerAccount")
    public ResponseEntity<Long> registerAccount(@RequestBody User user) {

        Long id = this.teoService.registerAccount(user.getLogin(), user.getPassword());

        if (!id.equals(0L)) {
            return new ResponseEntity<>(id, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(id, HttpStatus.CONFLICT);
        }
    }

}
