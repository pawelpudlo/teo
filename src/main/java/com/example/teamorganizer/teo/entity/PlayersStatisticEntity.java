package com.example.teamorganizer.teo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "PLAYERS_STATISTIC")
@Getter
@Setter
@NoArgsConstructor
public class PlayersStatisticEntity {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "STATISTIC_ID")
        private Long idStatistic;

        @Column(name = "PLAYER_ID")
        private Long idPlayer;

        @Column(name = "TRAINING")
        private Long training;

        @Column(name = "MATCHES")
        private Long matches;

        @Column(name = "GOALS")
        private Long goals;

        @Column(name = "ASSIST")
        private Long assist;

    public PlayersStatisticEntity(Long idPlayer, Long training, Long matches, Long goals, Long assist) {
        this.idPlayer = idPlayer;
        this.training = training;
        this.matches = matches;
        this.goals = goals;
        this.assist = assist;
    }
}
