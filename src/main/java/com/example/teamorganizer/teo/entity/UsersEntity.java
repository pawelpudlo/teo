package com.example.teamorganizer.teo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "USERS")
@NoArgsConstructor
@Getter
@Setter
public class UsersEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private Long idUser;

    @Column(name = "LOGIN")
    private String login;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "ROLE")
    private int role;

    public UsersEntity(String login, String password, int role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }
}

/*
CREATE TABLE USERS (
  USER_ID INTEGER PRIMARY KEY NOT NULL,
  LOGIN varchar(32) NOT NULL,
  PASSWORD varchar(64) NOT NULL,
  ROLE INTEGER NOT NULL
);
 */
