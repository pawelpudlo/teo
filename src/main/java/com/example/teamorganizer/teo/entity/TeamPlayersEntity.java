package com.example.teamorganizer.teo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "TEAMS_PLAYERS")
@Getter
@Setter
@NoArgsConstructor
public class TeamPlayersEntity {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "ID")
        private Long id;

        @Column(name = "TEAM_ID")
        private Long idTeam;

        @Column(name = "PLAYER_ID")
        private Long idPlayer;

        public TeamPlayersEntity(Long idTeam, Long idPlayer) {
            this.idTeam = idTeam;
            this.idPlayer = idPlayer;
        }
}
