package com.example.teamorganizer.teo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.*;

@Entity
@Table(name = "PLAYERS")
@Getter
@Setter
@NoArgsConstructor
public class PlayersEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PLAYER_ID")
    private Long idPlayer;

    @Column(name = "NAME")
    private String name;

    @Column(name = "FAMILYNAME")
    private String familyName;

    @Column(name = "AGE")
    private int age;

    public PlayersEntity(String name, String familyName, int age) {
        this.name = name;
        this.familyName = familyName;
        this.age = age;
    }

    public PlayersEntity(String name, String familyName) {
        this.name = name;
        this.familyName = familyName;
    }
}
