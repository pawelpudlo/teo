package com.example.teamorganizer.teo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "EVENT_TYPE")
@Getter
@Setter
@NoArgsConstructor
public class EventTypeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EVENT_TYPE_ID")
    private Long idEvent;

    @Column(name = "TYPE_NAME")
    private String name;

    public EventTypeEntity(String name) {
        this.name = name;
    }
}
