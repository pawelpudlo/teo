package com.example.teamorganizer.teo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "TEAMS")
@Getter
@Setter
@NoArgsConstructor
public class TeamsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TEAM_ID")
    private Long idTeam;

    @Column(name = "NAME_TEAM")
    private String nameTeam;

    @Column(name = "NUMBER_OF_PLAYERS")
    private int numbersOfPlayers;

    public TeamsEntity(String nameTeam, int numbersOfPlayers) {
        this.nameTeam = nameTeam;
        this.numbersOfPlayers = numbersOfPlayers;
    }
}

/*
CREATE TABLE TEAMS (
    TEAM_ID INTEGER PRIMARY KEY NOT NULL,
    NAME_TEAM VARCHAR (255) NOT NULL,
    NUMBER_OF_PLAYERS INTEGER NOT NULL
);
 */
