package com.example.teamorganizer.teo.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "SCHEDULES")
@Getter
@Setter
@NoArgsConstructor
public class SchedulesEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SCHEDULES_ID")
    private Long idSchedules;

    @Column(name = "TEAM_ID")
    private Long idTeam;

    @Column(name = "DATE")
    private Timestamp date;

    @Column(name = "EVENT_TYPE_ID")
    private Long idEventType;


    public SchedulesEntity(Long idTeam, Timestamp date,Long idEventType) {
        this.idTeam = idTeam;
        this.date = date;
        this.idEventType = idEventType;
    }

}
