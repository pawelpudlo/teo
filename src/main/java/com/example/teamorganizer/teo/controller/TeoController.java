package com.example.teamorganizer.teo.controller;

import com.example.teamorganizer.teo.dto.*;
import com.example.teamorganizer.teo.service.TeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RequestMapping("/api")
public class TeoController {

    private TeoService teoService;

    @Autowired
    public TeoController(TeoService teoService) {
        this.teoService = teoService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/addSchedule")
    public ResponseEntity<InformationDto> addSchedule(@RequestParam(value = "idTeam") Long idTeam,
                                                      @RequestParam(value = "date") String date,
                                                      @RequestParam(value = "typeEventId") Long typeEventId) {

        boolean value = this.teoService.addSchedule(idTeam, date, typeEventId);


        if (value) {
            return new ResponseEntity<>(new InformationDto("Dodałeś wydarzenie w terminarzu"), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new InformationDto("Wystąpił błąd"), HttpStatus.CONFLICT);
        }

    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/addTeam/{teamName}")
    public ResponseEntity<InformationDto> addTeam(@PathVariable(value = "teamName") String teamName) {

        boolean value = this.teoService.addTeam(teamName);


        if (value) {
            return new ResponseEntity<>(new InformationDto("Dodałeś drużynę"), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new InformationDto("Wystąpił błąd"), HttpStatus.CONFLICT);
        }

    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/loginAccount")
    public ResponseEntity<Long> loginAccount(@RequestParam(value = "login", required = false) String login,
                                                       @RequestParam(value = "password", required = false) String password) {

        Long id = this.teoService.loginAccount(login, password);

        if (!id.equals(0L)) {
            return new ResponseEntity<>(id, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(id, HttpStatus.CONFLICT);
        }

    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getEventOfSchedule/{schedule_id}")
    public ResponseEntity<EventOfScheduleDto> getEventOfSchedules(@PathVariable("schedule_id") Long schedule_id) {

        EventOfScheduleDto eventOfSchedule = teoService.getEventOfSchedules(schedule_id);
        return new ResponseEntity<>(eventOfSchedule, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getAllSchedule")
    public ResponseEntity<List<EventDto>> getAllSchedule() {

        List<EventDto> eventList = teoService.getAllScheduleEvent();
        return new ResponseEntity<>(eventList, HttpStatus.OK);
    }


    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getAllTeams")
    public ResponseEntity<List<TeamDto>> getTeams() {

        List<TeamDto> teamsDto = teoService.findAllTeams();
        return new ResponseEntity<>(teamsDto, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getSingleTeam/{teamId}")
    public ResponseEntity<?> getTeam(@PathVariable("teamId") Long teamId) {

        TeamDto teamDto = teoService.getSingleTeam(teamId);
        if (teamDto.getTeamId() != 0) {
            List<TeamDto> teamList = new ArrayList();
            teamList.add(teamDto);
            TeamsDto teamsDto = new TeamsDto(teamList);
            return new ResponseEntity<>(teamsDto, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new InformationDto("Podczas wyszukiwania zespołu wystapił błąd"), HttpStatus.CONFLICT);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getUserRole/{idUser}")
    public ResponseEntity<?> getUserRole(@PathVariable("idUser") Long idUser) {

        UserDto userDto = teoService.getUserRole(idUser);
        if (userDto.getIdUser() != 0) {
            return new ResponseEntity<>(userDto.getRole(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new InformationDto("Wystąpił błąd"), HttpStatus.CONFLICT);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/deleteUser/{idUser}")
    public ResponseEntity<InformationDto> deleteUser(@PathVariable("idUser") Long idUser) {

        boolean status = this.teoService.deleteUser(idUser);
        if (status) {
            return new ResponseEntity<>(new InformationDto("Użytkownik usunięty pomyślnie"), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new InformationDto("Wystąpił błąd"), HttpStatus.CONFLICT);
        }
    }


    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/ChangeRole")
    public ResponseEntity<InformationDto> addTeam(@RequestParam(value = "idUser", required = false) Long idUser,
                                                  @RequestParam(value = "role", required = false) int role) {

        boolean status = this.teoService.changeUserRole(idUser, role);
        if (status) {
            return new ResponseEntity<>(new InformationDto("Rola została zmieniona"), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new InformationDto("Wystąpił błąd"), HttpStatus.CONFLICT);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getAllUsers")
    public ResponseEntity<List<UserDto>> getAllUsers() {

        List<UserDto> userDto = this.teoService.getAllUsers();
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/addPlayer")
    public ResponseEntity<InformationDto> addPlayer(@RequestParam(value = "name", required = false) String name,
                                                    @RequestParam(value = "familyname", required = false) String familyname,
                                                    @RequestParam(value = "age", required = false) int age,
                                                    @RequestParam(value = "idTeam", required = false) Long idTeam) {

        boolean value = this.teoService.addPlayer(name, familyname, age, idTeam);

        if (value) {
            return new ResponseEntity<>(new InformationDto("Dodano zawodnika"), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new InformationDto("Błąd dodawania zawodnika"), HttpStatus.CONFLICT);
        }

    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getAllPlayersByTeam/{teamId}")
    public ResponseEntity<List<PlayerDto>> getAllPlayersByTeam(@PathVariable("teamId") Long teamId) {

        List<PlayerDto> players = this.teoService.getAllPlayersByTeam(teamId);
        return new ResponseEntity<>(players, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getSinglePlayer/{idPlayer}")
    public ResponseEntity<?> getSinglePlayerInformation(@PathVariable("idPlayer") Long idPlayer) {

        PlayerDto player = this.teoService.getSinglePlayerInformation(idPlayer);
        if (player.getIdPlayer() != 0) {
            return new ResponseEntity<>(player, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new InformationDto("Nie ma tego gracza w bazie"), HttpStatus.CONFLICT);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/changePlayerTeam")
    public ResponseEntity<InformationDto> changePlayersTeam(@RequestParam(value = "idPlayer", required = false) Long idPlayer,
                                                            @RequestParam(value = "idTeam", required = false) Long idTeam) {

        boolean status = this.teoService.changePlayersTeam(idPlayer, idTeam);
        if (status) {
            return new ResponseEntity<>(new InformationDto("Operacja się udała"), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new InformationDto("Wprowadzono błędne dane"), HttpStatus.CONFLICT);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getAllSchedulesByTeam/{idTeam}")
    public ResponseEntity<List<ScheduleDto>> getSchedules(@PathVariable("idTeam") Long idTeam) {

        return new ResponseEntity<>(this.teoService.getSchedulesByTeam(idTeam), HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/getAllTypesOfSchedule")
    public ResponseEntity<List<EventTypeDto>> getAllTypes() {

        return new ResponseEntity<>(this.teoService.getAllEvents(), HttpStatus.OK);
    }

    @PutMapping("/changeEventOfSchedule")
    public ResponseEntity<InformationDto> changeEvent(@RequestParam(value = "idSchedule", required = false) Long idSchedule,
                                                      @RequestParam(value = "idType", required = false) Long idType,
                                                      @RequestParam(value = "idTeam", required = false) Long idTeam,
                                                      @RequestParam(value = "date", required = false) String date) {

        Boolean status = this.teoService.changeEventOfSchedule(idSchedule, idType, idTeam, date);

        if (status) {
            return new ResponseEntity<>(new InformationDto("Zmiania wykonana"), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new InformationDto("Wprowadzono błędne dane"), HttpStatus.CONFLICT);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/deleteEventOfSchedule/{idSchedule}")
    public ResponseEntity<InformationDto> deleteEventOfSchedule(@PathVariable("idSchedule") Long idSchedule){

        Boolean status = this.teoService.deleteEventOfSchedule(idSchedule);

        if(status){
            return new ResponseEntity<>(new InformationDto("Operacja wykonana"), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(new InformationDto("Wprowadzono błędne dane"), HttpStatus.CONFLICT);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/deletePlayerOfTeam")
    public ResponseEntity<InformationDto> deletePlayerOfTeam(@RequestParam(value = "idPlayer", required = false) Long idPlayer,
                                                             @RequestParam(value = "idTeam", required = false) Long idTeam){

        Boolean status = this.teoService.deletePlayerOfTeam(idPlayer, idTeam);

        if(status){
            return new ResponseEntity<>(new InformationDto("Operacja wykonana"), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(new InformationDto("Wprowadzono błędne dane"), HttpStatus.CONFLICT);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/changePlayerStatistics")
    public ResponseEntity<InformationDto> changePlayerStatistics(@RequestParam(value = "idPlayer", required = false) Long idPlayer,
                                                                 @RequestParam(value = "training", required = false) Long training,
                                                                 @RequestParam(value = "match", required = false) Long match,
                                                                 @RequestParam(value = "assists", required = false) Long assists,
                                                                 @RequestParam(value = "goals", required = false) Long goals){

        Boolean status = this.teoService.changePlayerStatistics(idPlayer,training,match, assists, goals);

        if(status){
            return new ResponseEntity<>(new InformationDto("Operacja wykonana"), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(new InformationDto("Wprowadzono błędne dane"), HttpStatus.CONFLICT);
        }
    }

    @PutMapping("/updateAllStatistic")
    public ResponseEntity<InformationDto> addTeam(@RequestBody ForUpdateStatisticDto forUpdateStatisticDto) {

        /*
        JSON
        {
            "idSchedule": 1,
            "idTypeEvent": 1,
            "playerDto": [{"idPlayer":1,"training":99,"matches":1,"goals":1,"assist":3},{"idPlayer":2,"training":0,"matches":1,"goals":3,"assist":1}]
        }
         */

        boolean status = this.teoService.changeStatistic(forUpdateStatisticDto);
        if (status) {
            return new ResponseEntity<>(new InformationDto("Dane zostały zaktualizowane"), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new InformationDto("Wystąpił błąd"), HttpStatus.CONFLICT);
        }
    }

}
