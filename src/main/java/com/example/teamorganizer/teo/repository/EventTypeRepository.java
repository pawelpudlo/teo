package com.example.teamorganizer.teo.repository;

import com.example.teamorganizer.teo.entity.EventTypeEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventTypeRepository extends CrudRepository<EventTypeEntity,Long> {

    @Query("SELECT ETE.idEvent FROM EventTypeEntity as ETE WHERE ETE.idEvent=:idEvent")
    Long getEventId(@Param("idEvent") Long idEvent);

    @Query("SELECT ETE.name FROM EventTypeEntity ETE WHERE ETE.idEvent=:idEvent")
    String getEventName(@Param("idEvent") Long idEvent);

    @Query("SELECT ETE FROM EventTypeEntity ETE")
    List<EventTypeEntity> getAllEventsTypes();




}
