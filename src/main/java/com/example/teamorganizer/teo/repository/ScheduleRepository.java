package com.example.teamorganizer.teo.repository;

import com.example.teamorganizer.teo.entity.SchedulesEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleRepository extends CrudRepository<SchedulesEntity,Long> {

    @Query("SELECT SE FROM SchedulesEntity SE WHERE SE.idTeam=:idTeam")
    List<SchedulesEntity> getSchedulesEntityByIdTeam(@Param("idTeam") Long idTeam);

    @Query("SELECT SE FROM SchedulesEntity SE")
    List<SchedulesEntity> getSchedulesEntity();

    @Query("SELECT SE FROM SchedulesEntity SE WHERE SE.idSchedules=:idSchedules")
    SchedulesEntity getScheduleEntityByIdSchedules(@Param("idSchedules") Long idSchedules);

}
