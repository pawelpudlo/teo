package com.example.teamorganizer.teo.repository;

import com.example.teamorganizer.teo.dto.TeamDto;
import com.example.teamorganizer.teo.entity.TeamsEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.*;


@Repository
public interface TeamsRepository extends CrudRepository<TeamsEntity,Long> {

    @Query("Select te.nameTeam from TeamsEntity te")
    List<String> getAllTeams();

    @Query("Select te from TeamsEntity te")
    List<TeamsEntity> getAllTeamsWithAllInformation();

    TeamsEntity findOneByIdTeam(Long idTeam);

    @Query("SELECT TE.idTeam FROM TeamsEntity AS TE WHERE TE.idTeam=:idTeam")
    Long getTeamId(@Param("idTeam") Long idTeam);

    @Query("SELECT TE.numbersOfPlayers FROM TeamsEntity as TE WHERE TE.idTeam=:teamId")
    int getValuePlayerInTeam(@Param("teamId") Long teamId);

    @Query("SELECT TE FROM TeamsEntity as TE WHERE TE.idTeam=:teamId")
    TeamsEntity getOneByTeamId(@Param("teamId") Long teamId);

    @Query("SELECT TE FROM TeamsEntity as TE WHERE TE.idTeam=:idTeam")
    TeamsEntity getTeamInfoById(@Param("idTeam") Long idTeam);
}
