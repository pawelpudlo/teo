package com.example.teamorganizer.teo.repository;

import com.example.teamorganizer.teo.entity.TeamPlayersEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface TeamPlayersRepository extends CrudRepository<TeamPlayersEntity,Long> {

    @Query("SELECT TPE FROM TeamPlayersEntity TPE WHERE TPE.idTeam=:idTeam")
    List<TeamPlayersEntity> getAllPlayersByTeam(@Param("idTeam") Long idTeam);

    @Query("SELECT TPE FROM TeamPlayersEntity TPE WHERE TPE.idPlayer=:idPlayer")
    TeamPlayersEntity getTeamByPlayer(@Param("idPlayer") Long idPlayer);

    @Transactional
    @Modifying
    @Query("DELETE FROM TeamPlayersEntity TPE WHERE TPE.idPlayer=:idPlayer AND TPE.idTeam=:idTeam")
    void deleteByIdPlayerAndAndIdTeam(@Param("idPlayer") Long idPlayer,
                                      @Param("idTeam") Long idTeam);

    @Query("SELECT TPE.id FROM TeamPlayersEntity TPE WHERE TPE.idPlayer=:idPlayer AND TPE.idTeam=:idTeam")
    Long getIdForCheck(@Param("idPlayer") Long idPlayer,
                          @Param("idTeam") Long idTeam);
}
