package com.example.teamorganizer.teo.repository;

import com.example.teamorganizer.teo.dto.PlayerDto;
import com.example.teamorganizer.teo.entity.PlayersEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayersRepository extends CrudRepository<PlayersEntity,Long> {

    @Query("SELECT MAX(PE.idPlayer) FROM PlayersEntity as PE WHERE PE.name=:name AND PE.familyName=:familyname")
    Long getIdNewPlayer(@Param("name") String name,
                        @Param("familyname") String familyname);

    @Query("SELECT PE FROM PlayersEntity PE WHERE PE.idPlayer=:idPlayer")
    PlayersEntity getPlayerPersonalInformation(@Param("idPlayer") Long idPlayer);





}
