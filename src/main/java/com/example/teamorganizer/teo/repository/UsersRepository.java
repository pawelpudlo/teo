package com.example.teamorganizer.teo.repository;

import com.example.teamorganizer.teo.entity.TeamsEntity;
import com.example.teamorganizer.teo.entity.UsersEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends CrudRepository<UsersEntity,Long> {

    @Query("Select UE.login FROM UsersEntity as UE")
    List<String> getAllUsers();

    @Query("Select UE FROM UsersEntity as UE")
    List<UsersEntity> getAllUsersWithPassowrd();

    UsersEntity findOneByIdUser(Long idUser);

    @Query("Select UE FROM UsersEntity as UE")
    List<UsersEntity> getAllUsersWithAllInfromation();

}
