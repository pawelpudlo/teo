package com.example.teamorganizer.teo.repository;

import com.example.teamorganizer.teo.entity.PlayersEntity;
import com.example.teamorganizer.teo.entity.PlayersStatisticEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface PlayersStatisticRepository extends CrudRepository<PlayersStatisticEntity,Long> {

    @Query("SELECT PSE FROM PlayersStatisticEntity PSE WHERE PSE.idPlayer=:idPlayer")
    PlayersStatisticEntity getPlayerStatistics(@Param("idPlayer") Long idPlayer);

    @Transactional
    @Modifying
    @Query("DELETE FROM PlayersStatisticEntity PSE WHERE PSE.idPlayer=:idPlayer")
    void deletePlayersStatisticEntitiesByIdPlayer(@Param("idPlayer") Long idPlayer);



}
